import { Album } from "./album";

export class User {
    private album: Album[];

    constructor(private id: number,
                private username: string,
                private firstName: string,
                private isPro: boolean){
        this.album = [];
    }

    addAlbum(album: Album){
        this.album.push(album);
    }
    
    removeAlbum(album: Album): Album | undefined{
        const getIndex = this.album.findIndex(a => a.id === album.id);
        let deletedAlbum;
        if (getIndex >= 0){
            deletedAlbum = this.album[getIndex];
            this.album.splice(getIndex, 1);
        }
        return deletedAlbum;
    }
}