//import { User, Album, PhotoOrientation, Picture } from "./photo-app";

import { Album } from "./album";
import { PhotoOrientation } from "./photo-app";
import { Picture } from "./picture";
import { User } from "./user";

const user = new User(1, 'Mateo', 'Fabian', true);
const album = new Album (10, 'Pictures App');
const picture = new Picture (1, 'TypeScript Overviews', '2022-02', PhotoOrientation.Landscape);

user.addAlbum(album);
album.addPicture(picture);
console.log('user', user);;

user.removeAlbum(album);
console.log('user', user);;
3