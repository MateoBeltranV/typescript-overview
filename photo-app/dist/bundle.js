(() => {
  "use strict";
  var e = {
      987: (e, t, i) => {
        Object.defineProperty(t, "__esModule", { value: !0 }),
          (t.Album = void 0);
        const r = i(123);
        class s extends r.Item {
          constructor(e, t) {
            super(e, t), (this.picture = []);
          }
          addPicture(e) {
            this.picture.push(e);
          }
        }
        t.Album = s;
      },
      123: (e, t) => {
        Object.defineProperty(t, "__esModule", { value: !0 }),
          (t.Item = void 0),
          (t.Item = class {
            constructor(e, t) {
              (this.id = e), (this.title = t);
            }
          });
      },
      745: (e, t) => {
        var i;
        Object.defineProperty(t, "__esModule", { value: !0 }),
          (t.PhotoOrientation = void 0),
          ((i = t.PhotoOrientation || (t.PhotoOrientation = {}))[
            (i.Landscape = 0)
          ] = "Landscape"),
          (i[(i.Portrait = 1)] = "Portrait"),
          (i[(i.Square = 2)] = "Square"),
          (i[(i.Panorama = 3)] = "Panorama");
      },
      449: (e, t, i) => {
        Object.defineProperty(t, "__esModule", { value: !0 }),
          (t.Picture = void 0);
        const r = i(123);
        class s extends r.Item {
          constructor(e, t, i, r) {
            super(e, t), (this._date = i), (this._orientation = r);
          }
          toString() {
            return `[id: ${this.id}, title: ${this.title}, orientation: ${this._orientation}]`;
          }
        }
        t.Picture = s;
      },
      536: (e, t) => {
        Object.defineProperty(t, "__esModule", { value: !0 }),
          (t.User = void 0),
          (t.User = class {
            constructor(e, t, i, r) {
              (this.id = e),
                (this.username = t),
                (this.firstName = i),
                (this.isPro = r),
                (this.album = []);
            }
            addAlbum(e) {
              this.album.push(e);
            }
            removeAlbum(e) {
              const t = this.album.findIndex((t) => t.id === e.id);
              let i;
              return (
                t >= 0 && ((i = this.album[t]), this.album.splice(t, 1)), i
              );
            }
          });
      },
    },
    t = {};
  function i(r) {
    var s = t[r];
    if (void 0 !== s) return s.exports;
    var o = (t[r] = { exports: {} });
    return e[r](o, o.exports, i), o.exports;
  }
  (() => {
    const e = i(987),
      t = i(745),
      r = i(449),
      s = new (i(536).User)(1, "Mateo", "Fabian", !0),
      o = new e.Album(10, "Pictures App"),
      a = new r.Picture(
        1,
        "TypeScript Overviews",
        "2022-02",
        t.PhotoOrientation.Landscape
      );
    s.addAlbum(o),
      o.addPicture(a),
      console.log("user", s),
      s.removeAlbum(o),
      console.log("user", s);
  })();
})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlLmpzIiwibWFwcGluZ3MiOiJ3Q0FDQUEsT0FBT0MsZUFBZUMsRUFBUyxhQUFjLENBQUVDLE9BQU8sSUFDdERELEVBQVFFLFdBQVEsRUFDaEIsTUFBTUMsRUFBUyxFQUFRLEtBQ3ZCLE1BQU1ELFVBQWNDLEVBQU9DLEtBQ3ZCQyxZQUFZQyxFQUFJQyxHQUNaQyxNQUFNRixFQUFJQyxHQUNWRSxLQUFLQyxRQUFVLEdBRW5CQyxXQUFXRCxHQUNQRCxLQUFLQyxRQUFRRSxLQUFLRixJQUcxQlYsRUFBUUUsTUFBUUEsRyxZQ1poQkosT0FBT0MsZUFBZUMsRUFBUyxhQUFjLENBQUVDLE9BQU8sSUFDdERELEVBQVFJLFVBQU8sRUFPZkosRUFBUUksS0FOUixNQUNJQyxZQUFZQyxFQUFJQyxHQUNaRSxLQUFLSCxHQUFLQSxFQUNWRyxLQUFLRixNQUFRQSxLLFlDRnJCLElBQVdNLEVBSFhmLE9BQU9DLGVBQWVDLEVBQVMsYUFBYyxDQUFFQyxPQUFPLElBQ3RERCxFQUFRYSxzQkFBbUIsR0FFaEJBLEVBS1diLEVBQVFhLG1CQUFxQmIsRUFBUWEsaUJBQW1CLEtBSnpEQSxFQUE0QixVQUFJLEdBQUssWUFDdERBLEVBQWlCQSxFQUEyQixTQUFJLEdBQUssV0FDckRBLEVBQWlCQSxFQUF5QixPQUFJLEdBQUssU0FDbkRBLEVBQWlCQSxFQUEyQixTQUFJLEdBQUssWSxjQ1B6RGYsT0FBT0MsZUFBZUMsRUFBUyxhQUFjLENBQUVDLE9BQU8sSUFDdERELEVBQVFjLGFBQVUsRUFDbEIsTUFBTVgsRUFBUyxFQUFRLEtBQ3ZCLE1BQU1XLFVBQWdCWCxFQUFPQyxLQUN6QkMsWUFBWUMsRUFBSUMsRUFBT1EsRUFBT0MsR0FDMUJSLE1BQU1GLEVBQUlDLEdBQ1ZFLEtBQUtNLE1BQVFBLEVBQ2JOLEtBQUtPLGFBQWVBLEVBRXhCQyxXQUNJLE1BQU8sUUFBUVIsS0FBS0gsY0FBY0csS0FBS0YsdUJBQXVCRSxLQUFLTyxpQkFHM0VoQixFQUFRYyxRQUFVQSxHLFlDYmxCaEIsT0FBT0MsZUFBZUMsRUFBUyxhQUFjLENBQUVDLE9BQU8sSUFDdERELEVBQVFrQixVQUFPLEVBc0JmbEIsRUFBUWtCLEtBckJSLE1BQ0liLFlBQVlDLEVBQUlhLEVBQVVDLEVBQVdDLEdBQ2pDWixLQUFLSCxHQUFLQSxFQUNWRyxLQUFLVSxTQUFXQSxFQUNoQlYsS0FBS1csVUFBWUEsRUFDakJYLEtBQUtZLE1BQVFBLEVBQ2JaLEtBQUthLE1BQVEsR0FFakJDLFNBQVNELEdBQ0xiLEtBQUthLE1BQU1WLEtBQUtVLEdBRXBCRSxZQUFZRixHQUNSLE1BQU1HLEVBQVdoQixLQUFLYSxNQUFNSSxXQUFVQyxHQUFLQSxFQUFFckIsS0FBT2dCLEVBQU1oQixLQUMxRCxJQUFJc0IsRUFLSixPQUpJSCxHQUFZLElBQ1pHLEVBQWVuQixLQUFLYSxNQUFNRyxHQUMxQmhCLEtBQUthLE1BQU1PLE9BQU9KLEVBQVUsSUFFekJHLE1DcEJYRSxFQUEyQixHQUcvQixTQUFTQyxFQUFvQkMsR0FFNUIsSUFBSUMsRUFBZUgsRUFBeUJFLEdBQzVDLFFBQXFCRSxJQUFqQkQsRUFDSCxPQUFPQSxFQUFhakMsUUFHckIsSUFBSW1DLEVBQVNMLEVBQXlCRSxHQUFZLENBR2pEaEMsUUFBUyxJQU9WLE9BSEFvQyxFQUFvQkosR0FBVUcsRUFBUUEsRUFBT25DLFFBQVMrQixHQUcvQ0ksRUFBT25DLFEsTUNsQmYsTUFBTXFDLEVBQVUsRUFBUSxLQUNsQkMsRUFBYyxFQUFRLEtBQ3RCQyxFQUFZLEVBQVEsS0FFcEJDLEVBQU8sSUFERSxFQUFRLEtBQ0N0QixNQUFLLEVBQUcsUUFBUyxVQUFVLEdBQzdDSSxFQUFRLElBQUllLEVBQVFuQyxNQUFNLEdBQUksZ0JBQzlCUSxFQUFVLElBQUk2QixFQUFVekIsUUFBUSxFQUFHLHVCQUF3QixVQUFXd0IsRUFBWXpCLGlCQUFpQjRCLFdBQ3pHRCxFQUFLakIsU0FBU0QsR0FDZEEsRUFBTVgsV0FBV0QsR0FDakJnQyxRQUFRQyxJQUFJLE9BQVFILEdBRXBCQSxFQUFLaEIsWUFBWUYsR0FDakJvQixRQUFRQyxJQUFJLE9BQVFILEkiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9waG90by1hcHAvLi9zcmMvYWxidW0udHMiLCJ3ZWJwYWNrOi8vcGhvdG8tYXBwLy4vc3JjL2l0ZW0udHMiLCJ3ZWJwYWNrOi8vcGhvdG8tYXBwLy4vc3JjL3Bob3RvLWFwcC50cyIsIndlYnBhY2s6Ly9waG90by1hcHAvLi9zcmMvcGljdHVyZS50cyIsIndlYnBhY2s6Ly9waG90by1hcHAvLi9zcmMvdXNlci50cyIsIndlYnBhY2s6Ly9waG90by1hcHAvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vcGhvdG8tYXBwLy4vc3JjL21haW4udHMiXSwic291cmNlc0NvbnRlbnQiOlsiXCJ1c2Ugc3RyaWN0XCI7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcclxuZXhwb3J0cy5BbGJ1bSA9IHZvaWQgMDtcclxuY29uc3QgaXRlbV8xID0gcmVxdWlyZShcIi4vaXRlbVwiKTtcclxuY2xhc3MgQWxidW0gZXh0ZW5kcyBpdGVtXzEuSXRlbSB7XHJcbiAgICBjb25zdHJ1Y3RvcihpZCwgdGl0bGUpIHtcclxuICAgICAgICBzdXBlcihpZCwgdGl0bGUpO1xyXG4gICAgICAgIHRoaXMucGljdHVyZSA9IFtdO1xyXG4gICAgfVxyXG4gICAgYWRkUGljdHVyZShwaWN0dXJlKSB7XHJcbiAgICAgICAgdGhpcy5waWN0dXJlLnB1c2gocGljdHVyZSk7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0cy5BbGJ1bSA9IEFsYnVtO1xyXG4iLCJcInVzZSBzdHJpY3RcIjtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xyXG5leHBvcnRzLkl0ZW0gPSB2b2lkIDA7XHJcbmNsYXNzIEl0ZW0ge1xyXG4gICAgY29uc3RydWN0b3IoaWQsIHRpdGxlKSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xyXG4gICAgICAgIHRoaXMudGl0bGUgPSB0aXRsZTtcclxuICAgIH1cclxufVxyXG5leHBvcnRzLkl0ZW0gPSBJdGVtO1xyXG4iLCJcInVzZSBzdHJpY3RcIjtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xyXG5leHBvcnRzLlBob3RvT3JpZW50YXRpb24gPSB2b2lkIDA7XHJcbnZhciBQaG90b09yaWVudGF0aW9uO1xyXG4oZnVuY3Rpb24gKFBob3RvT3JpZW50YXRpb24pIHtcclxuICAgIFBob3RvT3JpZW50YXRpb25bUGhvdG9PcmllbnRhdGlvbltcIkxhbmRzY2FwZVwiXSA9IDBdID0gXCJMYW5kc2NhcGVcIjtcclxuICAgIFBob3RvT3JpZW50YXRpb25bUGhvdG9PcmllbnRhdGlvbltcIlBvcnRyYWl0XCJdID0gMV0gPSBcIlBvcnRyYWl0XCI7XHJcbiAgICBQaG90b09yaWVudGF0aW9uW1Bob3RvT3JpZW50YXRpb25bXCJTcXVhcmVcIl0gPSAyXSA9IFwiU3F1YXJlXCI7XHJcbiAgICBQaG90b09yaWVudGF0aW9uW1Bob3RvT3JpZW50YXRpb25bXCJQYW5vcmFtYVwiXSA9IDNdID0gXCJQYW5vcmFtYVwiO1xyXG59KShQaG90b09yaWVudGF0aW9uID0gZXhwb3J0cy5QaG90b09yaWVudGF0aW9uIHx8IChleHBvcnRzLlBob3RvT3JpZW50YXRpb24gPSB7fSkpO1xyXG4iLCJcInVzZSBzdHJpY3RcIjtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xyXG5leHBvcnRzLlBpY3R1cmUgPSB2b2lkIDA7XHJcbmNvbnN0IGl0ZW1fMSA9IHJlcXVpcmUoXCIuL2l0ZW1cIik7XHJcbmNsYXNzIFBpY3R1cmUgZXh0ZW5kcyBpdGVtXzEuSXRlbSB7XHJcbiAgICBjb25zdHJ1Y3RvcihpZCwgdGl0bGUsIF9kYXRlLCBfb3JpZW50YXRpb24pIHtcclxuICAgICAgICBzdXBlcihpZCwgdGl0bGUpO1xyXG4gICAgICAgIHRoaXMuX2RhdGUgPSBfZGF0ZTtcclxuICAgICAgICB0aGlzLl9vcmllbnRhdGlvbiA9IF9vcmllbnRhdGlvbjtcclxuICAgIH1cclxuICAgIHRvU3RyaW5nKCkge1xyXG4gICAgICAgIHJldHVybiBgW2lkOiAke3RoaXMuaWR9LCB0aXRsZTogJHt0aGlzLnRpdGxlfSwgb3JpZW50YXRpb246ICR7dGhpcy5fb3JpZW50YXRpb259XWA7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0cy5QaWN0dXJlID0gUGljdHVyZTtcclxuIiwiXCJ1c2Ugc3RyaWN0XCI7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcclxuZXhwb3J0cy5Vc2VyID0gdm9pZCAwO1xyXG5jbGFzcyBVc2VyIHtcclxuICAgIGNvbnN0cnVjdG9yKGlkLCB1c2VybmFtZSwgZmlyc3ROYW1lLCBpc1Bybykge1xyXG4gICAgICAgIHRoaXMuaWQgPSBpZDtcclxuICAgICAgICB0aGlzLnVzZXJuYW1lID0gdXNlcm5hbWU7XHJcbiAgICAgICAgdGhpcy5maXJzdE5hbWUgPSBmaXJzdE5hbWU7XHJcbiAgICAgICAgdGhpcy5pc1BybyA9IGlzUHJvO1xyXG4gICAgICAgIHRoaXMuYWxidW0gPSBbXTtcclxuICAgIH1cclxuICAgIGFkZEFsYnVtKGFsYnVtKSB7XHJcbiAgICAgICAgdGhpcy5hbGJ1bS5wdXNoKGFsYnVtKTtcclxuICAgIH1cclxuICAgIHJlbW92ZUFsYnVtKGFsYnVtKSB7XHJcbiAgICAgICAgY29uc3QgZ2V0SW5kZXggPSB0aGlzLmFsYnVtLmZpbmRJbmRleChhID0+IGEuaWQgPT09IGFsYnVtLmlkKTtcclxuICAgICAgICBsZXQgZGVsZXRlZEFsYnVtO1xyXG4gICAgICAgIGlmIChnZXRJbmRleCA+PSAwKSB7XHJcbiAgICAgICAgICAgIGRlbGV0ZWRBbGJ1bSA9IHRoaXMuYWxidW1bZ2V0SW5kZXhdO1xyXG4gICAgICAgICAgICB0aGlzLmFsYnVtLnNwbGljZShnZXRJbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWxldGVkQWxidW07XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0cy5Vc2VyID0gVXNlcjtcclxuIiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIlwidXNlIHN0cmljdFwiO1xyXG4vL2ltcG9ydCB7IFVzZXIsIEFsYnVtLCBQaG90b09yaWVudGF0aW9uLCBQaWN0dXJlIH0gZnJvbSBcIi4vcGhvdG8tYXBwXCI7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcclxuY29uc3QgYWxidW1fMSA9IHJlcXVpcmUoXCIuL2FsYnVtXCIpO1xyXG5jb25zdCBwaG90b19hcHBfMSA9IHJlcXVpcmUoXCIuL3Bob3RvLWFwcFwiKTtcclxuY29uc3QgcGljdHVyZV8xID0gcmVxdWlyZShcIi4vcGljdHVyZVwiKTtcclxuY29uc3QgdXNlcl8xID0gcmVxdWlyZShcIi4vdXNlclwiKTtcclxuY29uc3QgdXNlciA9IG5ldyB1c2VyXzEuVXNlcigxLCAnTWF0ZW8nLCAnRmFiaWFuJywgdHJ1ZSk7XHJcbmNvbnN0IGFsYnVtID0gbmV3IGFsYnVtXzEuQWxidW0oMTAsICdQaWN0dXJlcyBBcHAnKTtcclxuY29uc3QgcGljdHVyZSA9IG5ldyBwaWN0dXJlXzEuUGljdHVyZSgxLCAnVHlwZVNjcmlwdCBPdmVydmlld3MnLCAnMjAyMi0wMicsIHBob3RvX2FwcF8xLlBob3RvT3JpZW50YXRpb24uTGFuZHNjYXBlKTtcclxudXNlci5hZGRBbGJ1bShhbGJ1bSk7XHJcbmFsYnVtLmFkZFBpY3R1cmUocGljdHVyZSk7XHJcbmNvbnNvbGUubG9nKCd1c2VyJywgdXNlcik7XHJcbjtcclxudXNlci5yZW1vdmVBbGJ1bShhbGJ1bSk7XHJcbmNvbnNvbGUubG9nKCd1c2VyJywgdXNlcik7XHJcbjtcclxuMztcclxuIl0sIm5hbWVzIjpbIk9iamVjdCIsImRlZmluZVByb3BlcnR5IiwiZXhwb3J0cyIsInZhbHVlIiwiQWxidW0iLCJpdGVtXzEiLCJJdGVtIiwiY29uc3RydWN0b3IiLCJpZCIsInRpdGxlIiwic3VwZXIiLCJ0aGlzIiwicGljdHVyZSIsImFkZFBpY3R1cmUiLCJwdXNoIiwiUGhvdG9PcmllbnRhdGlvbiIsIlBpY3R1cmUiLCJfZGF0ZSIsIl9vcmllbnRhdGlvbiIsInRvU3RyaW5nIiwiVXNlciIsInVzZXJuYW1lIiwiZmlyc3ROYW1lIiwiaXNQcm8iLCJhbGJ1bSIsImFkZEFsYnVtIiwicmVtb3ZlQWxidW0iLCJnZXRJbmRleCIsImZpbmRJbmRleCIsImEiLCJkZWxldGVkQWxidW0iLCJzcGxpY2UiLCJfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18iLCJfX3dlYnBhY2tfcmVxdWlyZV9fIiwibW9kdWxlSWQiLCJjYWNoZWRNb2R1bGUiLCJ1bmRlZmluZWQiLCJtb2R1bGUiLCJfX3dlYnBhY2tfbW9kdWxlc19fIiwiYWxidW1fMSIsInBob3RvX2FwcF8xIiwicGljdHVyZV8xIiwidXNlciIsIkxhbmRzY2FwZSIsImNvbnNvbGUiLCJsb2ciXSwic291cmNlUm9vdCI6IiJ9
