"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let username;
username = 'mateo';
// tenemos una cadena, TS confia en mi!!
// let message: string = (username as string).length 
let message = username.length > 5 ?
    `Welcome ${username}` :
    'Username is too short';
console.log('message', message);
let usernameWithId = 'fabian';
//como obtener el username?
username = usernameWithId.substring(0, 3);
console.log(username);
