"use strict";
console.log('hola typescript');
//Number
//Explicito
let phone;
phone = 1;
//inferido (se asigna automaticamente)
let phoneNumber = 54234;
let hex = 0xf00d;
let binary = 0b1010;
let octal = 0o744;
//tipo: Boolean
//tipado Explicito
let isPro;
isPro = true;
//inferido
let isUserPro = false;
//String
let username = 'luixaviles';
username = "Luis";
//Template String, uso del  back-tick `
let userInfo;
userInfo = `
    User Info:
    username: ${username}
    firstName: ${username + ' Valencia'}
`;
console.log('userInfo', userInfo);
