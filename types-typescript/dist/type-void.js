"use strict";
//tipo explicito
function showInfo(user) {
    console.log('user Info', user.id, user.username, user.firstName);
    //return 'hola';
}
showInfo({ id: 1, username: 'mateo', firstName: 'Faboam' });
//tipo: Never
function handleError(code, message) {
    throw new Error(`${message}. Code: ${code}`);
}
