"use strict";
//orientación para fotograficas
// const landscape = 0;
// const portrait = 1;
// const square = 2;
// const panomara = 3;
var photoOrientation;
(function (photoOrientation) {
    photoOrientation[photoOrientation["landscape"] = 0] = "landscape";
    photoOrientation[photoOrientation["portrait"] = 1] = "portrait";
    photoOrientation[photoOrientation["square"] = 2] = "square";
    photoOrientation[photoOrientation["panomara"] = 3] = "panomara";
})(photoOrientation || (photoOrientation = {}));
const landscape = photoOrientation.landscape;
console.log(landscape);
console.log('landscape', photoOrientation[landscape]);
var pictureOrientation;
(function (pictureOrientation) {
    pictureOrientation[pictureOrientation["landscape"] = 10] = "landscape";
    pictureOrientation[pictureOrientation["portrait"] = 11] = "portrait";
    pictureOrientation[pictureOrientation["square"] = 12] = "square";
    pictureOrientation[pictureOrientation["panomara"] = 13] = "panomara";
})(pictureOrientation || (pictureOrientation = {}));
console.log('portrait', pictureOrientation.portrait);
var Country;
(function (Country) {
    Country["Bolivia"] = "bol";
    Country["Colombia"] = "col";
})(Country || (Country = {}));
const country = Country.Colombia;
console.log(country);
