// Crear una fotografía
// function createPicture(title, date, size){
//     //title
// }

type SquareSize = '100x100' | '500x500' |'1000x100';
// usamos Typescript y ademas definimos tipos para los parametros
// function createPicture( title: string, date: string, size: SquareSize){
//     //se crea la fotografica
//     console.log('create picture using', title, date, size);
// }

// createPicture('My Birthday', '17-09-2021', '1000x100'); 
// createPicture('My Birthday', '17-09-2021') // aquí se presenta un error porque la función requiere de todos los espacios
 
//PARAMETROS OPCIONALES EN FUNCIONES

function createPicture( title?: string, date?: string, size?: SquareSize){
    //se crea la fotografica
    console.log('create picture using', title, date, size);
}
createPicture('My Birthday', '17-09-2021'); 

// otra notación  Flat Array Function
let createPic = (title: string, date: string, size: SquareSize): object => {
    return { title, date, size }
};
const picture = createPic('platzi sesion', '2020-03-10', '100x100');
console.log(picture);

//tipo de retorno con TS
function handleError(code: number, message: string): never | string {
    //procesamiento del codigo, mensaje
    if( message == 'error'){
        throw new Error(`${message}. code error: ${code}`);
    } else {
        return 'An error has occurred';
    }
} //unión de tipos
let result = handleError(200, 'Ok');
console.log('result', result);

try {
    let result = handleError(404, 'error');
    console.log('result', result);
} catch (error) {
    console.log('ocurrio un error');
    
}
