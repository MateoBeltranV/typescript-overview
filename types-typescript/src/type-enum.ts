//orientación para fotograficas
// const landscape = 0;
// const portrait = 1;
// const square = 2;
// const panomara = 3;

enum photoOrientation {
    landscape,
    portrait,
    square,
    panomara
}

const landscape: photoOrientation = photoOrientation.landscape;
console.log(landscape);
console.log('landscape', photoOrientation[landscape]);

enum pictureOrientation {
    landscape = 10,
    portrait, // 11  va siendose consecutivo
    square,
    panomara
}
console.log('portrait', pictureOrientation.portrait);

enum Country {
    Bolivia = 'bol',
    Colombia = 'col'
}
const country: Country = Country.Colombia;
console.log(country);

