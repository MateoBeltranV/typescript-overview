// tipo explicito, usarlo cuando se se sabe muy bien el tipo de variable.
let idUser: any;
idUser = 1; // number
idUser = 's'; //string
console.log('idUser', idUser);

//inferido
let otherId;
idUser = 1; // number
idUser = 's'; //string
console.log('idUser', idUser);

let surprise: any = 'hello typescript';
const res = surprise.substring(6);
console.log('res', res);
