export {};
// //10 '10'
// let idUser: number | string;
// idUser = 10;
// idUser= '10';
// //buscar username dado un ID
// function getUsernameById(id: number | string){
//     return 'mateo'
// }
// console.log(getUsernameById(20));
// console.log(getUsernameById('20'));

//Alias de tipos
type idUser = number | string;
type userName = string;
let idUser: idUser;
idUser = 10;
idUser= '10';
//buscar username dado un ID
function getUsernameById(id: idUser): userName{
    return 'mateo'
}
console.log(getUsernameById(20));
console.log(getUsernameById('20'));

// TIPOS LITERALES
//100x100, 500x500, 1000x1000
type SquareSize = '100x100' | '500x500' | '1000x100';
// let smallPicture: SquareSize = '200x200'; //no puede poner el tamaño que no esté especificado
