export {};

let username: any;
username = 'mateo';

// tenemos una cadena, TS confia en mi!!
// let message: string = (username as string).length 
let message: string = (<string>username).length > 5 ?
                      `Welcome ${username}`:
                      'Username is too short';
console.log('message', message);

let usernameWithId: any = 'fabian';
//como obtener el username?
username = (<string>usernameWithId).substring(0,3);
console.log(username);

