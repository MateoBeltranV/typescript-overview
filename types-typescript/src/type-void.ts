//tipo explicito
function showInfo(user: any): any {
    console.log('user Info', user.id, user.username, user.firstName);
    //return 'hola';
    
}
showInfo({id: 1, username: 'mateo', firstName: 'Faboam'});

//tipo: Never

function handleError(code: number, message: string): never{

    throw new Error(`${message}. Code: ${code}`)
}