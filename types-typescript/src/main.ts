console.log('hola typescript');

//Number
//Explicito
let phone: number;
phone = 1;

//inferido (se asigna automaticamente)
let phoneNumber = 54234;

let hex: number = 0xf00d;
let binary: number = 0b1010;
let octal: number = 0o744; 

//tipo: Boolean
//tipado Explicito
let isPro: boolean;
isPro = true;

//inferido
let isUserPro = false;

//String
let username: String = 'luixaviles';
username = "Luis";

//Template String, uso del  back-tick `
let userInfo: string;
userInfo = `
    User Info:
    username: ${username}
    firstName: ${username + ' Valencia'}
`;
console.log('userInfo', userInfo);


